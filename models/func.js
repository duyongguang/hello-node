var fs = require('fs');
var path = require('path');
var _ = require('lodash');

var func= {
	'mktime':function() {
          var d = new Date(),
            r = arguments,
            i = 0,
            e = ['Hours', 'Minutes', 'Seconds', 'Month', 'Date', 'FullYear'];

          for (i = 0; i < e.length; i++) {
            if (typeof r[i] === 'undefined') {
              r[i] = d['get' + e[i]]();
              r[i] += (i === 3); // +1 to fix JS months.
            } else {
              r[i] = parseInt(r[i], 10);
              if (isNaN(r[i])) {
                return false;
              }
            }
          } 
          r[5] += (r[5] >= 0 ? (r[5] <= 69 ? 2e3 : (r[5] <= 100 ? 1900 : 0)) : 0);
          d.setFullYear(r[5], r[3] - 1, r[4]);
          d.setHours(r[0], r[1], r[2]);
          return (d.getTime() / 1e3 >> 0) - (d.getTime() < 0);
    },

	'strtotime':function(strings){
        var _ = strings.split(' ');
        var ymd = _[0];
        var hms = _[1];

        var str = ymd.split('-');
        var fix = hms.split(':');

        var year  = str[0] - 0; 
        var month = str[1] - 0 - 1; 
        var day   = str[2] - 0; 
        var hour   = fix[0] - 0; 
        var minute = fix[1] - 0; 
        var second = fix[2] - 0;

        time = (new Date(year, month, day, hour, minute, second)).getTime();
        return parseInt( time / 1000 );
    },

	'check_permission':function(operation,permission) {
            if(this.empty(permission)){
                    return false;
            }
            var permission_obj=JSON.parse(permission); 
            if(this.empty(permission_obj)==false && _.contains(permission_obj.permissions,operation)){
                    return true;
            }else{
                    return false;
            }
	},

	'time':function () {
	  		return Math.floor(new Date().getTime() / 1000);
	},

	'rand':function(min, max) {
			  var argc = arguments.length;
			  if (argc === 0) {
			    min = 0;
			    max = 2147483647;
			  } else if (argc === 1) {
			    throw new Error('Warning: rand() expects exactly 2 parameters, 1 given');
			  }
			  return Math.floor(Math.random() * (max - min + 1)) + min;
	},

	'log':function(content,req) {
		  if(req!=undefined){
				var content = req.ip+'--'+this.date('Y-m-d H:i:s',this.time())+'--'+req.headers['referer']+'--'+req.method+'--'+req.protocol+'://'+req.headers['host']+req.originalUrl+'--'+req.headers['user-agent']+'--'+content;
		  }
		  var logpath='./tmp/error.log';
		  content = content.toString().replace(/\r\n|\r/g, '\n'); // hack
		  var fd = fs.openSync(logpath, 'a+', 0666);
		  fs.writeSync(fd, content + '\n');
		  fs.closeSync(fd);
	},
    'empty':function(mixed_var) {
		  //   example 1: empty(null);
		  //   returns 1: true
		  //   example 2: empty(undefined);
		  //   returns 2: true
		  //   example 3: empty([]);
		  //   returns 3: true
		  //   example 4: empty({});
		  //   returns 4: true
		  //   example 5: empty({'aFunc' : function () { alert('humpty'); } });
		  //   returns 5: false
		  var undef, key, i, len;
		  var emptyValues = [undef, null, false, 0, '', '0'];

		  for (i = 0, len = emptyValues.length; i < len; i++) {
			if (mixed_var === emptyValues[i]) {
			  return true;
			}
		  }

		  if (typeof mixed_var === 'object') {
			for (key in mixed_var) {
			  // TODO: should we check for own properties only?
			  //if (mixed_var.hasOwnProperty(key)) {
			  return false;
			  //}
			}
			return true;
		  }

		  return false;
	},
	'date':function(format, timestamp) {
	  var that = this;
	  var jsdate, f;
	  // Keep this here (works, but for code commented-out below for file size reasons)
	  // var tal= [];
	  var txt_words = [
	    'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
	    'January', 'February', 'March', 'April', 'May', 'June',
	    'July', 'August', 'September', 'October', 'November', 'December'
	  ];
	  // trailing backslash -> (dropped)
	  // a backslash followed by any character (including backslash) -> the character
	  // empty string -> empty string
	  var formatChr = /\\?(.?)/gi;
	  var formatChrCb = function(t, s) {
	    return f[t] ? f[t]() : s;
	  };
	  var _pad = function(n, c) {
	    n = String(n);
	    while (n.length < c) {
	      n = '0' + n;
	    }
	    return n;
	  };
	  f = {
	    // Day
	    d: function() { // Day of month w/leading 0; 01..31
	      return _pad(f.j(), 2);
	    },
	    D: function() { // Shorthand day name; Mon...Sun
	      return f.l()
	        .slice(0, 3);
	    },
	    j: function() { // Day of month; 1..31
	      return jsdate.getDate();
	    },
	    l: function() { // Full day name; Monday...Sunday
	      return txt_words[f.w()] + 'day';
	    },
	    N: function() { // ISO-8601 day of week; 1[Mon]..7[Sun]
	      return f.w() || 7;
	    },
	    S: function() { // Ordinal suffix for day of month; st, nd, rd, th
	      var j = f.j();
	      var i = j % 10;
	      if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
	        i = 0;
	      }
	      return ['st', 'nd', 'rd'][i - 1] || 'th';
	    },
	    w: function() { // Day of week; 0[Sun]..6[Sat]
	      return jsdate.getDay();
	    },
	    z: function() { // Day of year; 0..365
	      var a = new Date(f.Y(), f.n() - 1, f.j());
	      var b = new Date(f.Y(), 0, 1);
	      return Math.round((a - b) / 864e5);
	    },

	    // Week
	    W: function() { // ISO-8601 week number
	      var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
	      var b = new Date(a.getFullYear(), 0, 4);
	      return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
	    },

	    // Month
	    F: function() { // Full month name; January...December
	      return txt_words[6 + f.n()];
	    },
	    m: function() { // Month w/leading 0; 01...12
	      return _pad(f.n(), 2);
	    },
	    M: function() { // Shorthand month name; Jan...Dec
	      return f.F()
	        .slice(0, 3);
	    },
	    n: function() { // Month; 1...12
	      return jsdate.getMonth() + 1;
	    },
	    t: function() { // Days in month; 28...31
	      return (new Date(f.Y(), f.n(), 0))
	        .getDate();
	    },

	    // Year
	    L: function() { // Is leap year?; 0 or 1
	      var j = f.Y();
	      return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
	    },
	    o: function() { // ISO-8601 year
	      var n = f.n();
	      var W = f.W();
	      var Y = f.Y();
	      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
	    },
	    Y: function() { // Full year; e.g. 1980...2010
	      return jsdate.getFullYear();
	    },
	    y: function() { // Last two digits of year; 00...99
	      return f.Y()
	        .toString()
	        .slice(-2);
	    },

	    // Time
	    a: function() { // am or pm
	      return jsdate.getHours() > 11 ? 'pm' : 'am';
	    },
	    A: function() { // AM or PM
	      return f.a()
	        .toUpperCase();
	    },
	    B: function() { // Swatch Internet time; 000..999
	      var H = jsdate.getUTCHours() * 36e2;
	      // Hours
	      var i = jsdate.getUTCMinutes() * 60;
	      // Minutes
	      var s = jsdate.getUTCSeconds(); // Seconds
	      return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
	    },
	    g: function() { // 12-Hours; 1..12
	      return f.G() % 12 || 12;
	    },
	    G: function() { // 24-Hours; 0..23
	      return jsdate.getHours();
	    },
	    h: function() { // 12-Hours w/leading 0; 01..12
	      return _pad(f.g(), 2);
	    },
	    H: function() { // 24-Hours w/leading 0; 00..23
	      return _pad(f.G(), 2);
	    },
	    i: function() { // Minutes w/leading 0; 00..59
	      return _pad(jsdate.getMinutes(), 2);
	    },
	    s: function() { // Seconds w/leading 0; 00..59
	      return _pad(jsdate.getSeconds(), 2);
	    },
	    u: function() { // Microseconds; 000000-999000
	      return _pad(jsdate.getMilliseconds() * 1000, 6);
	    },

	    // Timezone
	    e: function() { // Timezone identifier; e.g. Atlantic/Azores, ...
	      // The following works, but requires inclusion of the very large
	      // timezone_abbreviations_list() function.
	      /*              return that.date_default_timezone_get();
	       */
	      throw 'Not supported (see source code of date() for timezone on how to add support)';
	    },
	    I: function() { // DST observed?; 0 or 1
	      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
	      // If they are not equal, then DST is observed.
	      var a = new Date(f.Y(), 0);
	      // Jan 1
	      var c = Date.UTC(f.Y(), 0);
	      // Jan 1 UTC
	      var b = new Date(f.Y(), 6);
	      // Jul 1
	      var d = Date.UTC(f.Y(), 6); // Jul 1 UTC
	      return ((a - c) !== (b - d)) ? 1 : 0;
	    },
	    O: function() { // Difference to GMT in hour format; e.g. +0200
	      var tzo = jsdate.getTimezoneOffset();
	      var a = Math.abs(tzo);
	      return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
	    },
	    P: function() { // Difference to GMT w/colon; e.g. +02:00
	      var O = f.O();
	      return (O.substr(0, 3) + ':' + O.substr(3, 2));
	    },
	    T: function() {  
	      return 'UTC';
	    },
	    Z: function() { // Timezone offset in seconds (-43200...50400)
	      return -jsdate.getTimezoneOffset() * 60;
	    },

	    // Full Date/Time
	    c: function() { // ISO-8601 date.
	      return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
	    },
	    r: function() { // RFC 2822
	      return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
	    },
	    U: function() { // Seconds since UNIX epoch
	      return jsdate / 1000 | 0;
	    }
	  };
	  this.date = function(format, timestamp) {
	    that = this;
	    jsdate = (timestamp === undefined ? new Date() : // Not provided
	      (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
	      new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
	    );
	    return format.replace(formatChr, formatChrCb);
	  };
	  return this.date(format, timestamp);
	},

	'md5':function(str) { 
		  var xl;

		  var rotateLeft = function(lValue, iShiftBits) {
		    return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
		  };

		  var addUnsigned = function(lX, lY) {
		    var lX4, lY4, lX8, lY8, lResult;
		    lX8 = (lX & 0x80000000);
		    lY8 = (lY & 0x80000000);
		    lX4 = (lX & 0x40000000);
		    lY4 = (lY & 0x40000000);
		    lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
		    if (lX4 & lY4) {
		      return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		    }
		    if (lX4 | lY4) {
		      if (lResult & 0x40000000) {
		        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
		      } else {
		        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
		      }
		    } else {
		      return (lResult ^ lX8 ^ lY8);
		    }
		  };

		  var _F = function(x, y, z) {
		    return (x & y) | ((~x) & z);
		  };
		  var _G = function(x, y, z) {
		    return (x & z) | (y & (~z));
		  };
		  var _H = function(x, y, z) {
		    return (x ^ y ^ z);
		  };
		  var _I = function(x, y, z) {
		    return (y ^ (x | (~z)));
		  };

		  var _FF = function(a, b, c, d, x, s, ac) {
		    a = addUnsigned(a, addUnsigned(addUnsigned(_F(b, c, d), x), ac));
		    return addUnsigned(rotateLeft(a, s), b);
		  };

		  var _GG = function(a, b, c, d, x, s, ac) {
		    a = addUnsigned(a, addUnsigned(addUnsigned(_G(b, c, d), x), ac));
		    return addUnsigned(rotateLeft(a, s), b);
		  };

		  var _HH = function(a, b, c, d, x, s, ac) {
		    a = addUnsigned(a, addUnsigned(addUnsigned(_H(b, c, d), x), ac));
		    return addUnsigned(rotateLeft(a, s), b);
		  };

		  var _II = function(a, b, c, d, x, s, ac) {
		    a = addUnsigned(a, addUnsigned(addUnsigned(_I(b, c, d), x), ac));
		    return addUnsigned(rotateLeft(a, s), b);
		  };

		  var convertToWordArray = function(str) {
		    var lWordCount;
		    var lMessageLength = str.length;
		    var lNumberOfWords_temp1 = lMessageLength + 8;
		    var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
		    var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
		    var lWordArray = new Array(lNumberOfWords - 1);
		    var lBytePosition = 0;
		    var lByteCount = 0;
		    while (lByteCount < lMessageLength) {
		      lWordCount = (lByteCount - (lByteCount % 4)) / 4;
		      lBytePosition = (lByteCount % 4) * 8;
		      lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount) << lBytePosition));
		      lByteCount++;
		    }
		    lWordCount = (lByteCount - (lByteCount % 4)) / 4;
		    lBytePosition = (lByteCount % 4) * 8;
		    lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
		    lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
		    lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
		    return lWordArray;
		  };

		  var wordToHex = function(lValue) {
		    var wordToHexValue = '',
		      wordToHexValue_temp = '',
		      lByte, lCount;
		    for (lCount = 0; lCount <= 3; lCount++) {
		      lByte = (lValue >>> (lCount * 8)) & 255;
		      wordToHexValue_temp = '0' + lByte.toString(16);
		      wordToHexValue = wordToHexValue + wordToHexValue_temp.substr(wordToHexValue_temp.length - 2, 2);
		    }
		    return wordToHexValue;
		  };

		  var x = [],
		    k, AA, BB, CC, DD, a, b, c, d, S11 = 7,
		    S12 = 12,
		    S13 = 17,
		    S14 = 22,
		    S21 = 5,
		    S22 = 9,
		    S23 = 14,
		    S24 = 20,
		    S31 = 4,
		    S32 = 11,
		    S33 = 16,
		    S34 = 23,
		    S41 = 6,
		    S42 = 10,
		    S43 = 15,
		    S44 = 21;

		  str = this.utf8_encode(str);
		  x = convertToWordArray(str);
		  a = 0x67452301;
		  b = 0xEFCDAB89;
		  c = 0x98BADCFE;
		  d = 0x10325476;

		  xl = x.length;
		  for (k = 0; k < xl; k += 16) {
		    AA = a;
		    BB = b;
		    CC = c;
		    DD = d;
		    a = _FF(a, b, c, d, x[k + 0], S11, 0xD76AA478);
		    d = _FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
		    c = _FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
		    b = _FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
		    a = _FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
		    d = _FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
		    c = _FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
		    b = _FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
		    a = _FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
		    d = _FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
		    c = _FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
		    b = _FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
		    a = _FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
		    d = _FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
		    c = _FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
		    b = _FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
		    a = _GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
		    d = _GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
		    c = _GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
		    b = _GG(b, c, d, a, x[k + 0], S24, 0xE9B6C7AA);
		    a = _GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
		    d = _GG(d, a, b, c, x[k + 10], S22, 0x2441453);
		    c = _GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
		    b = _GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
		    a = _GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
		    d = _GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
		    c = _GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
		    b = _GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
		    a = _GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
		    d = _GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
		    c = _GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
		    b = _GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
		    a = _HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
		    d = _HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
		    c = _HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
		    b = _HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
		    a = _HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
		    d = _HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
		    c = _HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
		    b = _HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
		    a = _HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
		    d = _HH(d, a, b, c, x[k + 0], S32, 0xEAA127FA);
		    c = _HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
		    b = _HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
		    a = _HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
		    d = _HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
		    c = _HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
		    b = _HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
		    a = _II(a, b, c, d, x[k + 0], S41, 0xF4292244);
		    d = _II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
		    c = _II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
		    b = _II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
		    a = _II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
		    d = _II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
		    c = _II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
		    b = _II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
		    a = _II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
		    d = _II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
		    c = _II(c, d, a, b, x[k + 6], S43, 0xA3014314);
		    b = _II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
		    a = _II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
		    d = _II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
		    c = _II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
		    b = _II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
		    a = addUnsigned(a, AA);
		    b = addUnsigned(b, BB);
		    c = addUnsigned(c, CC);
		    d = addUnsigned(d, DD);
		  }

		  var temp = wordToHex(a) + wordToHex(b) + wordToHex(c) + wordToHex(d);

		  return temp.toLowerCase();
	},

	'nl2br':function(str, is_xhtml) {
		  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display

		  return (str + '')
		    .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
	},

	'addslashes':function(str) {
 			return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
	},

	'stripslashes':function(str) {
		  return (str + '').replace(/\\(.?)/g, function(s, n1) {
		      switch (n1) {
		        case '\\':
		          return '\\';
		        case '0':
		          return '\u0000';
		        case '':
		          return '';
		        default:
		          return n1;
		      }
		    });
	},

	'trim':function(str, charlist) {
 		  var whitespace, l = 0,
		    i = 0;
		  str += '';

		  if (!charlist) {
		    // default list
		    whitespace =
		      ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
		  } else {
		    // preg_quote custom list
		    charlist += '';
		    whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
		  }

		  l = str.length;
		  for (i = 0; i < l; i++) {
		    if (whitespace.indexOf(str.charAt(i)) === -1) {
		      str = str.substring(i);
		      break;
		    }
		  }

		  l = str.length;
		  for (i = l - 1; i >= 0; i--) {
		    if (whitespace.indexOf(str.charAt(i)) === -1) {
		      str = str.substring(0, i + 1);
		      break;
		    }
		  }

		  return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
	},
	 
	'utf8_decode':function(str_data) {
		  var tmp_arr = [],
		    i = 0,
		    ac = 0,
		    c1 = 0,
		    c2 = 0,
		    c3 = 0,
		    c4 = 0;

		  str_data += '';

		  while (i < str_data.length) {
		    c1 = str_data.charCodeAt(i);
		    if (c1 <= 191) {
		      tmp_arr[ac++] = String.fromCharCode(c1);
		      i++;
		    } else if (c1 <= 223) {
		      c2 = str_data.charCodeAt(i + 1);
		      tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
		      i += 2;
		    } else if (c1 <= 239) {
		      // http://en.wikipedia.org/wiki/UTF-8#Codepage_layout
		      c2 = str_data.charCodeAt(i + 1);
		      c3 = str_data.charCodeAt(i + 2);
		      tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
		      i += 3;
		    } else {
		      c2 = str_data.charCodeAt(i + 1);
		      c3 = str_data.charCodeAt(i + 2);
		      c4 = str_data.charCodeAt(i + 3);
		      c1 = ((c1 & 7) << 18) | ((c2 & 63) << 12) | ((c3 & 63) << 6) | (c4 & 63);
		      c1 -= 0x10000;
		      tmp_arr[ac++] = String.fromCharCode(0xD800 | ((c1 >> 10) & 0x3FF));
		      tmp_arr[ac++] = String.fromCharCode(0xDC00 | (c1 & 0x3FF));
		      i += 4;
		    }
		  }

		  return tmp_arr.join('');
	},

	'utf8_encode':function(argString) {
	  if (argString === null || typeof argString === 'undefined') {
	    return '';
	  }

	  var string = (argString + ''); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
	  var utftext = '',
	    start, end, stringl = 0;

	  start = end = 0;
	  stringl = string.length;
	  for (var n = 0; n < stringl; n++) {
	    var c1 = string.charCodeAt(n);
	    var enc = null;

	    if (c1 < 128) {
	      end++;
	    } else if (c1 > 127 && c1 < 2048) {
	      enc = String.fromCharCode(
	        (c1 >> 6) | 192, (c1 & 63) | 128
	      );
	    } else if ((c1 & 0xF800) != 0xD800) {
	      enc = String.fromCharCode(
	        (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      );
	    } else { // surrogate pairs
	      if ((c1 & 0xFC00) != 0xD800) {
	        throw new RangeError('Unmatched trail surrogate at ' + n);
	      }
	      var c2 = string.charCodeAt(++n);
	      if ((c2 & 0xFC00) != 0xDC00) {
	        throw new RangeError('Unmatched lead surrogate at ' + (n - 1));
	      }
	      c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
	      enc = String.fromCharCode(
	        (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
	      );
	    }
	    if (enc !== null) {
	      if (end > start) {
	        utftext += string.slice(start, end);
	      }
	      utftext += enc;
	      start = end = n + 1;
	    }
	  }

	  if (end > start) {
	    utftext += string.slice(start, stringl);
	  }

	  return utftext;
	},

	'number_format':function(number, decimals, dec_point, thousands_sep) {
	  number = (number + '')
	    .replace(/[^0-9+\-Ee.]/g, '');
	  var n = !isFinite(+number) ? 0 : +number,
	    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	    s = '',
	    toFixedFix = function(n, prec) {
	      var k = Math.pow(10, prec);
	      return '' + (Math.round(n * k) / k)
	        .toFixed(prec);
	    };
	  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
	  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
	    .split('.');
	  if (s[0].length > 3) {
	    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	  }
	  if ((s[1] || '')
	    .length < prec) {
	    s[1] = s[1] || '';
	    s[1] += new Array(prec - s[1].length + 1)
	      .join('0');
	  }
	  return s.join(dec);
	},
	'strip_tags':function(input, allowed) {
	  allowed = (((allowed || '') + '')
	    .toLowerCase()
	    .match(/<[a-z][a-z0-9]*>/g) || [])
	    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
	  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
	    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
	  return input.replace(commentsAndPhpTags, '')
	    .replace(tags, function($0, $1) {
	      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
	    });
	},

	'stripUnicode' : function(s){
			return s.replace(/[^\x20-\x7E]/g,"");
	},

	'html_encode':function(s){ 
        return s.replace(/"|&|'|<|>|[\x00-\x20]|[\x7F-\xFF]|[\u0100-\u2700]/g, 
                      function($0){
                          var c = $0.charCodeAt(0), r = ["&#"];
                          c = (c == 0x20) ? 0xA0 : c;
                          r.push(c); r.push(";");
                          return r.join("");
                      });
    },

    'html_decode':function(s){ //&#34966;&#24773;&#20387;     &#119;&#119;&#119;&#46;&#120;
        var HTML_DECODE = {"&lt;"  : "<", "&gt;"  : ">", "&amp;" : "&", "&nbsp;": " ", "&quot;": "\"", "&apos;": "'"  } , 
        REGX_NUM = /\d{1,}/;
 
        return  s.replace(/&\w{1,};|&#\d{1,};/g ,
                      function($0){
                          var c = HTML_DECODE[$0];
                          if(c == undefined){
                              // Maybe is Entity Number
                              var m = $0.match(REGX_NUM);
                              if(m){
                                  var cc = m[0];
                                  cc = (cc == 160) ? 32: cc;
                                  c = String.fromCharCode(cc);
                              }else{
                                  c = $0;
                              }
                          }
                          return c;
                      });  
    },

	'str2unicode':function(str){ 
			return escape(str).replace(/%/g,
				function () { 
					return "\\"; 
				}).toLowerCase();
	},

	'unicode2str':function(str) { //  \u5c0616   \u8fdb   \u5236  \u8868  \u793a  \u4e3a   \u6587   \u5b57
			return unescape(str);
	},

	'remove_xss':function(source){
			var xss = require('xss'); 
			var whiteList = {
				  a: ['target', 'href', 'title','name'],
				  abbr: ['title'],
				  address: [],
				  area: ['shape', 'coords', 'href', 'alt'],
				  article: [],
				  aside: [],
				  audio: ['autoplay', 'controls', 'loop', 'preload', 'src'],
				  b: [],
				  bdi: ['dir'],
				  bdo: ['dir'],
				  big: [],
				  blockquote: ['cite','align'],
				  br: [],
				  caption: [],
				  center: [],
				  cite: [],
				  code: [],
				  col: ['align', 'valign', 'span', 'width'],
				  colgroup: ['align', 'valign', 'span', 'width'],
				  dd: [],
				  del: ['datetime'],
				  details: ['open'],
				  div: ['align'],
				  dl: [],
				  dt: [],
				  em: [],
				  embed : ['src', 'width', 'height', 'type', 'loop', 'autostart', 'quality', 'align', 'allowscriptaccess'],
				  font: ['color', 'size', 'face'],
				  footer: [],
				  h1: ['align'],
				  h2: ['align'],
				  h3: ['align'],
				  h4: ['align'],
				  h5: ['align'],
				  h6: ['align'],
				  header: [],
				  hr: [],
				  i: [],
				  img: ['src', 'alt', 'title', 'width', 'height','border','align'],
				  ins: ['datetime'],
				  li: ['align'],
				  mark: [],
				  nav: [],
				  ol: ['align'],
				  p: ['align'],
				  pre: [],
				  s: [],
				  sub: [],
				  sup: [],
				  strike:[],
				  section:[],
				  small: [],
				  span: ['align'],
				  strong: [],
				  table: ['width', 'height', 'border', 'align', 'valign', 'cellspacing', 'cellpadding', 'bordercolor'],
				  tbody: ['align', 'valign'],
				  td: ['width', 'height', 'colspan', 'align', 'valign','rowspan', 'bgcolor'],
				  tfoot: ['align', 'valign'],
				  th: ['width', 'height', 'colspan', 'align', 'valign','rowspan', 'bgcolor'],
				  thead: ['align', 'valign'],
				  tr: ['rowspan', 'align', 'valign'],
				  tt: [],
				  u: [],
				  ul: ['align'],
				  video: ['autoplay', 'controls', 'loop', 'preload', 'src', 'height', 'width']
				};
			var html = xss(source, {
			  whiteList:whiteList,
			  stripIgnoreTag: true
			});
			return html;
	},
	'mkdirs':function(dirpath, mode, callback) {
	    fs.exists(dirpath, function(exists) {
	        if(exists) {
	                callback(dirpath);
	        } else {
	                //尝试创建父目录，然后再创建当前目录
	                func.mkdirs(path.dirname(dirpath), mode, function(){
	                        fs.mkdir(dirpath, mode, callback);
	                });
	        }
	    });
	},
	'parameter':function(args,columns){
                var params={};
                var offset=0;
                var limit=10;
                var order='id';
                var sort='desc'; 

                _.forEach(args, function(value,key){
                        if(_.indexOf(columns, key)!==-1){
                                params[key]=value;
                        }
                        
                        if(key==='limit' &&  value>=0){
                                limit=value;
                        }else if(key==='offset' &&  value>=0){
                                offset=value;
                        }else if(key==='order' &&  _.indexOf(columns, value)!==-1 ){
                                order=value;
                        }else if(key==='sort' &&  _.indexOf(['asc','desc'], value)!==-1 ){
                                sort=value;
                        }
                });
                var sequence={}; sequence[order]=sort;
                return {params:params,limit:limit,offset:offset,sort:sequence};
    }











}

module.exports = func;
