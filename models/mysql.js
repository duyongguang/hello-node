
var Func = require('./func'); 
var mysql= {
	
 	 'save':function(table,data,callback){
			var name="";
			var value="";
			for (var x in data) {
					if(name!==""){
							name+=" , ";
							value+=" , ";
					}
					name+=x;
					value+="'"+Func.addslashes(data[x])+"'";
			};
			var sql = "insert into "+table+"("+name+")values("+value+")";
			this.query(sql,function(err,result){
					if(err || result==undefined){
							callback(err,undefined);
					}else{
							callback(err,result.insertId);
					}
			}); 
 	  },
 	 'update':function(table,condition,data,callback){
			var set="";
			for (var y in data) {
					if(set!==""){
							set+=" , ";
					}
					set+=y+"='"+Func.addslashes(data[y])+"'";
			};

			var sql =  "update  " + table +" set "+ set ;
			sql+=this.compose_where(condition);

			this.query(sql,function(err,result){
					if(err || result==undefined){
							callback(err,undefined);
					}else{
							callback(err,result.affectedRows);
					}
			}); 
 	  },

 	  'del':function(table,condition,callback){
  				var sql =  "delete from  " + table ;
 	  			sql+=this.compose_where(condition); 
                
                this.query(sql,function(err,result){
                   		if(err || result==undefined){
								callback(err,undefined);
						}else{
								callback(err,result.affectedRows);
						}
                }); 
 	  },

	  'findOne':function(table,condition,callback){
  				var sql =  "select * from  " + table ;
 	  			sql+=this.compose_where(condition); 
 	  			sql+=" limit 1 "; 

                this.query(sql,function(err,result){
                		if(err || result==undefined){
								callback(err,undefined);
						}else{
								callback(err,result[0]);
						}
                }); 
 	  },

 	  'findAll':function(table,field,condition,sort,offset,limit,callback){
 	  			var colume= this.compose_field(field); 
 	  			var sql =  "select " +colume+ " from  " + table ; 

 	  			sql+=this.compose_where(condition); 
 				sql+=this.compose_sort(sort); 

                sql+=" limit  "+offset+" , "+limit;

                this.query(sql,function(err,result){
                  		if(err || result==undefined){
								callback(err,undefined);
						}else{
								callback(err,result);
						}
                }); 
 	  },
 	  'findCount':function(table,field,condition,callback){
 	  			var sql =  "select count("+field[0]+") as count from  " + table;

 	  			sql+=this.compose_where(condition);

          this.query(sql,function(err,result){
	            		if(err || result==undefined){
												callback(err,undefined);
									}else{
												callback(err,result[0]['count']);
									}
          }); 
 	  },
 	  'compose_where':function(condition){
				if( condition!==undefined && condition!=='' ){
		                var where = "";
		                for (var x in condition) {
									if(where!==""){
												where+=" and ";
									}

			                		if( typeof(condition[x]) !== "object" ){
												// {'id':'xxx',user_id:'yyy'}  
												where+=x+"='"+Func.addslashes(condition[x])+"'";
			                		}else{
			                					//{{'name':'id','term':'=','value':'xxx'},{'name':'user_id','term':'=','value':'yyy'}}
			                					where+=condition[x]['name']+condition[x]['term']+"'"+Func.addslashes(condition[x]['value'])+"'";
			                		}	                       
		                };
		                if(where!=""){
		                			return " where "+ where ;
		                }
	            }
	            return '';
 	  },
 	  'compose_sort':function(sort){			
	            if( sort!==undefined && sort!=="" ){
	            		var sql=""; 
		                for (var y in sort) {
		                		if(sql!==""){
			            				sql+=" , ";
			            		}
		                		sql+=y +" "+ sort[y];
		                }
		                if(sql!=""){
		                		return " order by "+sql;
		                }
		                
	            }
	        	return '';
 	  },
 	  'compose_field':function(field){			
	            if( field!==undefined && field!=="" ){
	            		var colume=""; 
		                for (var y in field) {
		                		if(colume!==""){
			            				colume+=" , ";
			            		}
		                		colume+= field[y];
		                }
		                if(colume!=""){
		                		return  colume;
		                }
	            }
	        	return '*';
 	  },
 	  'query':function(sql,callback){
 	  		var mysql  = require('mysql');
 	  		var Config = require('../config');
 	  		
			var db_config = {
			  	host: Config.database.host,
				user: Config.database.user,
				password: Config.database.password,
				database: Config.database.database
			};

			var connection;

			function handleDisconnect() {
			  connection = mysql.createConnection(db_config); // Recreate the connection, since
			                                                  // the old one cannot be reused.
			  connection.connect(function(err) {              // The server is either down
			    if(err) {                                     // or restarting (takes a while sometimes).
			      console.log('error when connecting to db:', err);
			      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
			    }                                     // to avoid a hot loop, and to allow our node script to
			  });                                     // process asynchronous requests in the meantime.
			                                          // If you're also serving http, display a 503 error.
			  connection.on('error', function(err) {
			    console.log('db error', err);
			    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
			      handleDisconnect();                         // lost due to either server restart, or a
			    } else {                                      // connnection idle timeout (the wait_timeout
			      throw err;                                  // server variable configures this)
			    }
			  });
			}

			handleDisconnect();

			connection.query(sql, function(err, rows, fields) {
				  //if (err) throw err;
				  if(err){
				  		callback(err,undefined);
				  }else{
				  		callback(err,rows);
				  }
			}); 
			connection.end();
 	  }
}

module.exports = mysql;