var db=require('./mysql');
var Comment={
     'table':'comments',

     'fields':['id','article_id','content','created'],
     
     'query':function(sql,callback){
         		 db.query(sql,function(err,result){
         		 		  callback(err,result);
         		 }); 
     },
     'save':function(data,callback){ 
         		 db.save(this.table,data,function(err,result){
         		 		  callback(err,result);
         		 }); 
     },
     'update':function(condition,data,callback){ 
            db.update(this.table,condition,data,function(err,result){
                  callback(err,result);
            }); 
     },
     'del':function(condition,callback){
            db.del(this.table,condition,function(err,result){
                  callback(err,result);
            }); 
     },
     'findOne':function(condition,callback){
            db.findOne(this.table,condition,function(err,result){
                  callback(err,result);
            }); 
     },
     'findAll':function(field,condition,sort,offset,limit,callback){
            db.findAll(this.table,field,condition,sort,offset,limit,function(err,result){
                  callback(err,result);
            }); 
     },
     'findCount':function(field,condition,callback){
            db.findCount(this.table,field,condition,function(err,result){
                  callback(err,result);
            }); 
     }
}

module.exports=Comment;
