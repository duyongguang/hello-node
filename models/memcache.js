var memcache= {
	
	'produce':function(){
			var Memcached = require('memcached');
			var Config = require('../config');
			var memcached = new Memcached(Config.memcache.host+":"+Config.memcache.port);
			return memcached;
	},

	'set':function(key,value,time,callback){ 
			var memcached = this.produce();
			memcached.set(key,value, time, function( err, result ){
                      if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,result);
					  }
                      memcached.end(); 
            });
 
	},

	'get':function(key,callback){
			var memcached = this.produce();
			memcached.get(key, function( err, data ){
                      if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,data);
					  }
                      memcached.end(); 
            });
 
	},

	'del':function(key,callback){
			var memcached = this.produce();
			memcached.del(key, function( err, result ){
                      if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,result);
					  }
                      memcached.end(); 
            });
 
	},

	'update':function(key,value,time,callback){
			var memcached = this.produce();
			memcached.replace(key,value, time, function( err, result ){
                      if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,result);
					  }
                      memcached.end(); 
            });
 
	},


}

module.exports = memcache;