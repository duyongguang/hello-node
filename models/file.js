var file= {

	'set':function(key,value,time,callback){
			var fs = require('fs');
			var expires=  Date.now() + time*1000;
			var content = expires+'|'+value;
			fs.writeFile('./tmp/'+key, content,'utf8',function (err) {
	                  if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,true);
					  }
            });
	},

	'get':function(key,callback){
			var fs = require('fs');
			var now =  Date.now();

			fs.exists('./tmp/'+key, function(exists){
				 if(exists){
							fs.readFile('./tmp/'+key, 'utf8', function (err,data) {
								      if(err){
									  			callback(err,undefined);
									  }else{
										  		var index=data.indexOf("|");
							                    var expires=data.substring(0,index);
							                    var content=data.substring(index+1);
							                    
							                    if(now > expires){
							                    		callback(err,false);
							                    }else{
							                    		callback(err,content);
							                    }
									  }
							});
				 }else{
				    		callback(null,false);
				 }
			});
	},

	'del':function(key,callback){
			var fs = require('fs');

			fs.exists('./tmp/'+key, function(exists){
				 if(exists){
			    		fs.unlink('./tmp/'+key, function (err) {
						    	  if(err){
								  		callback(err,undefined);
								  }else{
								  		callback(err,true);
								  }
						});
				 }else{
				    	callback(null,true);
				 }
			});
	},

	'update':function(key,value,time,callback){
			var fs = require('fs');
			var expires=  Date.now() + time*1000; //need to get real expires in file
			var content = expires+'|'+value;
			fs.writeFile('./tmp/'+key, content,'utf8',function (err) {
					  if(err){
					  		callback(err,undefined);
					  }else{
					  		callback(err,true);
					  }
            });
 
	},


}

module.exports = file;