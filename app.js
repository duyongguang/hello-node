
/**
 * Module dependencies.
 * api  http://expressjs.jser.us/api.html
 */

var express = require('express');
var http = require('http');
var path = require('path');
var fs = require('fs');
var _ = require('lodash');
var partials = require('express-partials');
var multiparty = require('connect-multiparty');
var app = express();

var Config = require('./config');
// all environments
app.set('port', Config.port);
app.set('env',Config.env);
app.enable('trust proxy');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(partials());
app.use(multiparty());
app.use(express.static(path.join(__dirname, 'static')));
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.cookieParser('cookie_'+Config.secret));
//app.use(express.session());
app.use(express.cookieSession( {key:"session_id",secret:'session_'+Config.secret,cookie: {domain: Config.domain, path: '/' , httpOnly:true , expires:new Date(Date.now()+30*24*3600*1000)}} ));


//do sth before request 
var Func = require('./models/func');
app.use('/',function(req, res, next){
        //user auth
        if(req.session.user_id!== undefined && req.session.user_id!== ''){
              var login=true;
        }else{
              var login=false;
        }
        req.login=login; //in controllers, login=req.login;  
        res.locals.login = login;  //in views, <%=login%>;
        res.locals.session = req.session;  //in views, <%=session.username%>;
        res.locals.Func = Func;    //in views, <%=Func.time()%>
        res.locals._ = _; //in views, <% _.indexOf() %> 

        //csrf get
        if(req.method==='GET' && !req.xhr ){ 
              var token_session = req.session.token;
              if(token_session==undefined || token_session ==''){
                      var token_session=Func.md5(Func.rand()); 
                      req.session.token=token_session;
              }
              res.locals.token = Func.md5(token_session+Config.secret);  // when we post data,we must post '<input type="hidden" name="token" value="<%=token%>" />' together ,even AJAX post     
        }
        //csrf post,except kindeditor upload image
        if(req.method==='POST'  &&  req.path!='/kindeditor/upload'){
              var token_view=req.body.token; 
              var token_session=req.session.token;
              if(token_view==undefined || token_session==undefined || Func.md5(token_session+Config.secret) !== token_view ){
                    return res.send('token must be post');
              }
        }
        //xss post
        if(req.method==='POST'){
               for(x in req.body){
                      if(x.indexOf('html')===-1){
                            req.body[x]=Func.html_decode(req.body[x]);
                            req.body[x]=Func.strip_tags(req.body[x]);
                      }else{
                            req.body[x]=Func.remove_xss(req.body[x]);
                      }
               }
        }

        //show user detail,when get every page
        if(req.method==='GET' && login  &&  !req.xhr ){
              var User = require('./models/user');
              var condition={'id':req.session.user_id};
              User.findOne(condition,function(err,result){
                      res.locals.userdetail = result; 
                      next(); 
              });
        }else{
                      next(); 
        }    
});

app.use(app.router);



// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


//import all controllers
var controller_path = './controllers';
fs.readdirSync(controller_path).forEach(function (file){
	require(controller_path + '/' + file)(app); 
})


http.createServer(app).listen(app.get('port'), function(){
  	console.log('Express server listening on port ' + app.get('port'));
});
