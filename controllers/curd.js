module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user'); 
    var Cache = require('../models/'+Config.cache);
    var Func = require('../models/func'); 
    var async = require('async');

    app.get('/user/add', function(req, res){
                    //console.log(req.login);
    				res.render('user/add');
    });

    app.post('/user/add', function(req, res){
    				var username=req.body.username;
    				var email=req.body.email;
    				var password=req.body.password;
    				var created= parseInt(new Date().getTime()/1000,10); 

    				var data={'username':username,'email':email,'password':password,'created':created};

        			User.save(data,function(err,uid){
                            if(err || uid==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{
                                    res.redirect('user/view/'+uid);
                            }
                    });
    });

	app.get('/user/edit/:id', function(req, res){
					var uid=req.params.id;
			        var condition={'id':uid};
                    User.findOne(condition,function(err,detail){
                            if(err || detail==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/edit', { detail: detail});
                            } 
                    }); 
    });

    app.post('/user/edit/:id', function(req, res){
    				var uid=req.params.id;
    				var username=req.body.username;
    				var email=req.body.email; 

    				var condition={'id':uid};
    				var data={'username':username,'email':email};

        			User.update(condition,data,function(err,affectedRows){
                            if(err || affectedRows==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{
                                    res.redirect('user/view/'+uid);
                            }
                    });
    });

	app.get('/user/del/:id', function(req, res){
					var uid=req.params.id;
					var condition={'id':uid};
                    User.del(condition,function(err,affectedRows){
                            if(err || affectedRows==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{
                                    res.redirect('user/list');
                            }
                    });
    });

    app.get('/user/view/:id', function(req, res){
    				var uid=req.params.id;
			        var condition={'id':uid};
                    User.findOne(condition,function(err,detail){
                            if(err || detail==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/view', { detail: detail});
                            }
                    });
			        
    });

	app.get('/user/list/:page?', function(req, res){ 
                    if(req.params.page && req.params.page>0){
                            var page= req.params.page;
                    }else{
                            var page= 1;
                    }
                    
                    

                    var limit=3;
                    var offset=(page-1)*limit;

                    //var field='';
                    var field=['username','email'];

                    //var condition={'id':'0','created':'1395034068'};
					var condition=[{'name':'id','term':'>','value':'0'},{'name':'created','term':'>=','value':'1395034068'}];
                    var sort = {'id':'asc','created':'asc'};
                    
                    async.parallel({
                        users: function(callback){
                                User.findAll(field,condition,sort,offset,limit,function(err,results){
                                    callback(err,results);
                                });
                        },
                        members: function(callback){
                                var sql="select count(1) as num from users ";
                                User.query(sql,function(err,results){
                                        callback(err,results);
                                });
                        }
                    },
                    function(err, results) {
                            if(err || results['users']==undefined || results['members']==undefined){
                                    Func.log(err,req);
                                    res.render('404', { error: err});
                            }else{  
                                    res.render('user/list', { userlist: results['users'] ,allpage: results['members'][0]['num'] ,pageurl:'/user/list/',nowpage: page,perpage: limit});
                            }
                    });

                   

                    
    });

    app.get('/user/all', function(req, res){ 
                    var sql="select * from users ";
                    User.query(sql,function(err,userlist){
                                    if(err || userlist==undefined){
                                            Func.log(err,req);
                                            res.render('404', { error: err});
                                    }else{
                                            res.render('user/list', { userlist: userlist,allpage: 23 ,pageurl:'/user/list/',nowpage: 3,perpage:5 }); 
                                    }
                    });
    });


}
