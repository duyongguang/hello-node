module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user'); 
    var Cache = require('../models/'+Config.cache);
    var Func = require('../models/func'); 
    var async = require('async');
 
    app.get('/cookie/add', function(req, res){
                res.cookie('user_id', '78225', {domain: Config.domain, path: '/' , httpOnly:true , expires:new Date(Date.now()+30*24*3600*1000) });  
                res.send({'result':'success'});
    });

    app.get('/cookie/get', function(req, res){
                res.send({'result':req.cookies.user_id});
    });

    app.get('/cookie/delete', function(req, res){
                res.clearCookie('user_id', {domain: Config.domain});
                res.send({'result':'success'});
    });
 

}
