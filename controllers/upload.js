module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user');
    var request = require("request");
    var querystring = require('querystring');
    var Func = require('../models/func');

     

    app.get('/upload', function(req, res){
            res.render('index/upload', { title: 'upload' });
    });


    app.post('/upload', function(req, res){
                 var fs = require('fs');
                 fs.readFile(req.files.image.path, function(err, data){
                        var ext = req.files.image.name.split('.').pop();
                        var new_path = '/images/'+Func.date('Y/m/d/');
                        var new_dir ='./static'+new_path;
                        var new_name = Func.date('His')+'_'+Func.rand(10000,99999)+'.'+ext; 

                        Func.mkdirs(new_dir, 0755, function(reanhlpath){
                                  fs.writeFile(new_dir+new_name, data, function(err){
                                        if(err){
                                              res.send({'error':1,'message':'上传文件失败。'});
                                        }else{
                                              res.send({'error':0,'url': new_path+new_name });
                                        }
                                  })
                        });
                  });


    });
    // user gm to crop image,must install imagemagick on server
    //http://www.imagemagick.org/
    //https://github.com/aheckmann/gm/tree/master/examples
    app.post('/upload/resize', function(req, res){
                 var fs = require('fs');
                 fs.readFile(req.files.image.path, function(err, data){
                        var ext = req.files.image.name.split('.').pop();
                        var new_path = '/images/'+Func.date('Y/m/d/');
                        var new_dir ='./static'+new_path;
                        var new_name = Func.date('His')+'_'+Func.rand(10000,99999)+'.'+ext; 
                        Func.mkdirs(new_dir, 0755, function(reanhlpath){
                                    var gm = require('gm').subClass({ imageMagick: true });
                                    /* resize to 200* 100
                                    gm(req.files.image.path).resize(200, 100).write(new_dir+new_name, function (err) {
                                            if(err){
                                                  res.send({'error':1,'message':'上传文件失败。'});
                                            }else{
                                                  res.send({'error':0,'url': new_path+new_name });
                                            }
                                    }); 
                                    */

                                    /* resize and crop to 150 * 150
                                    gm(req.files.image.path).gravity("Center").thumb(150,150,new_dir+new_name, function(err){
                                            if(err){
                                                  res.send({'error':1,'message':'上传文件失败。'});
                                            }else{
                                                  res.send({'error':0,'url': new_path+new_name });
                                            }
                                    }); 
                                    */


                                    /* force resize to  150 *150
                                    gm(req.files.image.path).resize(150, 150, '!').write(new_dir+new_name, function(err){
                                            if(err){
                                                    res.send({'error':1,'message':'上传文件失败。'});
                                            }else{
                                                    res.send({'error':0,'url': new_path+new_name });
                                            }
                                    });
                                    */

                                    /* create a static image,then thumb it to small */
                                    gm().in(req.files.image.path).mosaic().write(new_dir+new_name, function (err) {
                                            if(err){
                                                    res.send({'error':1,'message':'上传文件失败。'});
                                            }else{
                                                        gm(new_dir+new_name).gravity("Center").thumb(150,150,new_dir+new_name+'thumb.jpg', function(err){
                                                                if(err){
                                                                      res.send({'error':1,'message':'上传文件失败。'});
                                                                }else{
                                                                      res.send({'error':0,'url': new_path+new_name+'thumb.jpg' });
                                                                }
                                                        });
                                            }
                                    }); 
                                     //*/


                                    gm(req.files.image.path).format(function(err, value){
                                        console.log(value);
                                    })

                                    /* 转换格式，gif to png
                                    gm(req.files.image.path).resize(150, 150).stream('png', function (err, stdout, stderr) {
                                            if(err){
                                                    res.send({'error':1,'message':'上传文件失败。'});
                                            }else{
                                                    new_name=new_name+'.png';
                                                    var writeStream = fs.createWriteStream(new_dir+new_name);
                                                    stdout.pipe(writeStream);
                                                    res.send({'error':0,'url': new_path+new_name });
                                            }
                                    });
                                    */

                        });
                  });
    });

     

}
