module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user'); 
    var Cache = require('../models/'+Config.cache);
    var Func = require('../models/func'); 
    var async = require('async');

    app.get('/session/add', function(req, res){
                 req.session.uid=78225; 
                 res.send({'result':'success'});
    });

    app.get('/session/get', function(req, res){ 
                res.send({'result':req.session.uid });
    });

    app.get('/session/delete', function(req, res){
                delete req.session.uid; 
                res.send({'result':'success'});
    });
}
