module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user');
    var request = require("request");
    var querystring = require('querystring');
    var Func = require('../models/func');


     app.get('/mail', function(req, res){
                var nodemailer = require("nodemailer");
                var smtpTransport = require('nodemailer-smtp-transport');
                var transport = nodemailer.createTransport(smtpTransport({
                    host: 'smtp.exmail.qq.com',
                    port: 465,
                    secure:true,
                    auth: {
                        user: 'no-reply@qq.com',
                        pass: 'xxx'
                    },
                    maxConnections: 5,
                    maxMessages: 10
                }));
                
                var mailOptions = {
                        from: 'xxx <no-reply@qq.com>', // sender address
                        to: 'xxx@126.com,xxx@qq.com,xxx@gmail.com', // list of receivers
                        subject: 'title', // Subject line
                        //text: 'Hello world  ', // plaintext body
                        html: '<div style="color:#0088cc;font-size:20px;">内容 <a href="http://baidu.com">链接</a></div>' // html body
                }; 
                
                transport.sendMail(mailOptions, function(error, info){
                    if(error){
                        res.send(error);
                    }else{
                        res.send('Message sent: ' + info.response);
                    }
                });
    });


    app.get('/gmail', function(req, res){
            var nodemailer = require('nodemailer');
            
            // create reusable transporter object using SMTP transport
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'xxx@gmail.com',
                    pass: 'xxx'
                }
            });

            // NB! No need to recreate the transporter object. You can use
            // the same transporter object for all e-mails

            // setup e-mail data with unicode symbols
            var mailOptions = {
                from: 'xxx ✔ <xxx@gmail.com>', // sender address
                to: 'xxx@126.com, yyy@126.com', // list of receivers
                subject: 'Hello ✔', // Subject line
                text: 'Hello world ✔', // plaintext body
                html: '<b>Hello world ✔</b>' // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function(error, info){
                if(error){
                    res.send(error);
                }else{
                    res.send('Message sent: ' + info.response);
                }
            });
    });

     

}
