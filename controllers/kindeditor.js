module.exports = function (app) {
    var Func = require('../models/func');

    
    app.get('/editor', function(req, res){
            res.render('index/editor', { title: 'editor' });
    });

    app.post('/kindeditor/upload', function(req, res){
                 var fs = require('fs');
                 fs.readFile(req.files.imgFile.path, function(err, data){
                        var ext = req.files.imgFile.type.split('/').pop();
                        var new_path = '/images/'+Func.date('Y/m/d/');
                        var new_dir ='./static'+new_path;
                        var new_name = Func.date('His')+'_'+Func.rand(10000,99999)+'.'+ext; 

                        Func.mkdirs(new_dir, 0755, function(reanhlpath){
                                  fs.writeFile(new_dir+new_name, data, function(err){
                                        if(err){
                                              res.send({'error':1,'message':'上传文件失败。'});
                                        }else{
                                              res.send({'error':0,'url': new_path+new_name });
                                        }
                                  })
                        });
                  });
    });

    app.post('/editor', function(req, res){
                    var html_content=req.body.html_content;  
                    res.send(html_content);

    });

}
