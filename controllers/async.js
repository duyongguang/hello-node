module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user'); 
    var Cache = require('../models/'+Config.cache);
    var Func = require('../models/func'); 
    var async = require('async');
    //example https://github.com/freewind/async_demo

    
    app.get('/async/series', function(req, res){ 
                    //var field='';
                    var field=['username','email'];
                    //var condition={'id':'0','created':'1395034068'};
                    var condition=[{'name':'id','term':'>','value':'0'},{'name':'created','term':'>=','value':'1395034068'}];
                    var sort = {'id':'asc','created':'asc'};
                    var limit=5;
                    var offset=0;

                    async.series([
                            function(callback){
                                    User.findAll(field,condition,sort,offset,limit,function(err,results){
                                            callback(err,results);
                                    });
                            },  
                            function(callback){
                                    var sql="select * from users order by id asc limit 2; ";
                                    User.query(sql,function(err,results){
                                            callback(err,results);
                                    });
                            }
                    ], function (err, results) { 
                            if(err || results[0]==undefined || results[1]==undefined){
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/list', { userlist: results[0] ,users: results[1]});
                            }
                    });         
    });

	app.get('/async/parallel', function(req, res){ 
                    //var field='';
                    var field=['username','email'];
                    //var condition={'id':'0','created':'1395034068'};
					var condition=[{'name':'id','term':'>','value':'0'},{'name':'created','term':'>=','value':'1395034068'}];
                    var sort = {'id':'asc','created':'asc'};
                    var limit=5;
                    var offset=0;

                    async.parallel([ 
                            function(callback){
                                    User.findAll(field,condition,sort,offset,limit,function(err,results){
                                            callback(err,results);
                                    });
                            },  
                            function(callback){
                                    var sql="select * from users order by id asc limit 2; ";
                                    User.query(sql,function(err,results){
                                            callback(err,results);
                                    });
                            }
                    ], function (err, results) { 
                            if(err || results[0]==undefined || results[1]==undefined){
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/list', { userlist: results[0] ,users: results[1]});
                            }
                    }); 
    });

    app.get('/async/waterfall', function(req, res){ 
                    //var field='';
                    var field=['username','email'];
                    //var condition={'id':'0','created':'1395034068'};
                    var condition=[{'name':'id','term':'>','value':'0'},{'name':'created','term':'>=','value':'1395034068'}];
                    var sort = {'id':'desc','created':'asc'};
                    var limit=5;
                    var offset=5;

                    async.waterfall([
                            function(callback){
                                    var sql='select * from users where id in (1) ';
                                    User.query(sql,function(err,result_1){
                                            callback(err,result_1);
                                    });
                            },
                            function(result_1, callback){ 
                                    var new_id=result_1[0].id;
                                    
                                    var sql='select * from users where id in (2,'+new_id+')';
                                    User.query(sql,function(err,result_2){
                                            callback(err,result_2); 
                                    });
                            },
                            function(result_2, callback){
                                    var new_id_1=result_2[0].id;
                                    var new_id_2=result_2[1].id;
                                     
                                    var sql='select * from users where id in (3,'+new_id_1+','+new_id_2+')';
                                    User.query(sql,function(err,result_3){
                                            callback(err,result_3);
                                    });
                            }
                    ], function (err, result_3) {
                            if(err || result_3==undefined ){
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/list', { userlist: result_3 ,users: result_3 });
                            }
                    });
    });

    app.get('/async/map', function(req, res){ 
                    async.waterfall([
                            function(callback){
                                    var sql='select * from users  limit 5 ';
                                    User.query(sql,function(err,result_1){
                                            callback(err,result_1);
                                    });
                            },
                            function(result_1, callback){ 
                                        async.map(result_1, function(item, callback) {  
                                                    var uid=item.id;
                                                    var sql="select username from users where id='"+uid+"' ";
                                                    User.query(sql,function(err,res){
                                                            var username=res[0].username;
                                                            //into username_list
                                                            callback(err,username);
                                                    });
                                        }, function(err, username_list) {
                                                    //into each item
                                                    for(var i=0;i<result_1.length;i++){
                                                            result_1[i]['username']=result_1[i]['username']+'----'+username_list[i];
                                                    }
                                                    //second callback of waterfall 
                                                    callback(err,result_1); 
                                        });
                            }
                    ], function (err, result_2) {
                            if(err || result_2==undefined ){
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/list', { userlist: result_2 ,users: result_2 });
                            }
                    });
    });  


    app.get('/async/foreach', function(req, res){
                    async.waterfall([
                            function(callback){
                                    var sql='select * from users  limit 5 ';
                                    User.query(sql,function(err,result_1){
                                            callback(err,result_1);
                                    });
                            },
                            function(result_1, callback){ 
                                        async.forEach(result_1,function(item, callback) {
                                                    var uid=item.id;
                                                    Cache.del(uid,function(err,result){ 
                                                             callback(err);
                                                    }); 
                                        }, function(err) {
                                                    var result_2=result_1;
                                                    //second callback of waterfall 
                                                    callback(err,result_2);
                                        });
                            }
                    ], function (err, result_2) {
                            if(err || result_2==undefined ){
                                    res.render('404', { error: err});
                            }else{
                                    res.render('user/list', { userlist: result_2 ,users: result_2 });
                            }
                    });
    });  



}
