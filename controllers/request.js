module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user');
    var request = require("request");
    var querystring = require('querystring');
    var Func = require('../models/func');

    app.get('/api/get', function(req, res){
                    var options = {
                        client_id:Config.api.client_id,
                        client_secret:Config.api.client_secret,
                        email:'duyongguang@yeeyan.cn'
                    };
                    var params = querystring.stringify(options); 
                    request({
                          uri: Config.api.client_host+"/users?"+params,
                          encoding:'utf8',
                          timeout: 10000, //10 second 
                          method: "GET"
                    }, function(error, response, body) {
                          if (!error && response.statusCode == 200) { 
                            res.send(body);
                          }
                    }); 

    });

    app.get('/api/post', function(req, res){
                        request({
                              uri: Config.api.client_host+"/payments",
                              encoding:'utf8',
                              timeout: 10000, //10 second 
                              method: "POST",
                              form: {
                                    client_id:Config.api.client_id,
                                    client_secret:Config.api.client_secret,
    								user_id: "2" ,
    								order_id: "2" ,
    								bank_id: "2" ,
    								bank_sn: "2" ,
    								pay_amount: "23"
                              }
                        }, function(error, response, body) {
                              if (!error && response.statusCode == 200) { 
                                res.send(body);
                              }
                        });

    });
    app.get('/api/put', function(req, res){
                        request({
                              uri: Config.api.client_host+"/orders/1",
                              encoding:'utf8',
                              timeout: 10000, //10 second 
                              method: "PUT",
                              form: {
                                  client_id:Config.api.client_id,
                                  client_secret:Config.api.client_secret,
                                  status: "deleted" 
                              }
                        }, function(error, response, body) {
                              if (!error && response.statusCode == 200) { 
                                res.send(body);
                              }
                        });

    });
    app.get('/api/delete', function(req, res){ 
                    var options = {
                        client_id:Config.api.client_id,
                        client_secret:Config.api.client_secret,
                        name:'duyongguang',
                        email:'yongguangdu@gmail.com'
                    };
                    var params = querystring.stringify(options);
                    request({
                          uri: Config.api.client_host+"/test/delete?"+params,
                          encoding:'utf8',
                          timeout: 10000, //10 second 
                          method: "DELETE"
                    }, function(error, response, body) {
                          if (!error && response.statusCode == 200) { 
                            res.send(body);
                          }
                    }); 

    });



}
