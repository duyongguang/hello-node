module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user');
    var request = require("request");
    var querystring = require('querystring');
    var Func = require('../models/func');

    app.get('/login', function(req, res){
            var message=req.session.message;
            if(message===undefined || message===null){
                    message='login please';
            }else{
                    delete req.session.message;
            }
            res.render('index/login',{ message: message });
    });

    app.post('/login', function(req, res){  
                    var email=req.body.email;
                    var password=req.body.password; 
                    var condition={'email':email,'password':password};
                    User.findOne(condition,function(err,detail){
                             if(detail!==undefined){
                                    req.session.user_id=detail.id;
                                    res.redirect('user/view/'+detail.id);  
                             }else{
                                    req.session.message='username or password is not match!';
                                    res.redirect('/login');  
                             }
                    });
    });

    app.get('/', function(req, res){
            res.render('index/index', { title: 'homepage' });
    });

     

}
