module.exports = function (app) {
    app.get('/mongo/create', function(req, res){
                 var User = require('../mg_models/user'); 

                 var data = { username : 'duyongguang', email : 'duyongguang@126.com',avatar : '/img/medium_default.png', created : '2014-07-23 12:12:12'};
           	     User.create(data, function(error,last_id){
                	    if(error){
                	        res.send(error);
                	    }else{ 
                            res.send(last_id);
                	    }
            	});
    });

    app.get('/mongo/update', function(req, res){
                var User = require('../mg_models/user'); 

                var conditions = {username : 'duyongguang'};
                var update  = {$set : {email : 'duyongguang@sina.cn'}};
                var options  = {upsert:false,multi:true};//是否修改多个
                User.update(conditions, update, options, function(error){
                        if(error) {
                                res.send(error);
                        } else { 
                                res.send('update ok');
                        }
                });
    });

    app.get('/mongo/remove', function(req, res){
                 var User = require('../mg_models/user'); 

            	 var conditions = {username: 'duyongguang'};
              	 User.remove(conditions, function(error){
                	    if(error) {
                	        res.send(error);
                	    } else { 
                            res.send('remove ok');
                	    }
              	 });
    });

    app.get('/mongo/findone', function(req, res){
                 var User = require('../mg_models/user');

            	 var conditions = {username : 'duyongguang'}; 
            	 var fields   = 'username email avatar created'; 
            	 User.findOne(conditions, fields, function(error, result){
                	    if(error){
                	        res.send(error);
                	    }else{
                            res.send(result);
                	    }
            	 });
    });

    app.get('/mongo/query', function(req, res){
                var User = require('../mg_models/user');

            	User.userList(function(error, result){
                	    if(error){
                	        res.send(error);
                	    }else{
                            res.send(result);
                	    }
            	 });
    });

}
