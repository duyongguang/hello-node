module.exports = function (app) {
    var Config = require('../config');
    var User = require('../models/user'); 
    var Cache = require('../models/'+Config.cache);
    var Func = require('../models/func'); 
    var async = require('async');

    app.get('/user/cache/write', function(req, res){ 
                            Cache.set('3333333333',"aaaaaaaaaaaaaaa",3600,function(err,result){
                                    if(err || result==undefined){
                                            res.render('404', { error: err});
                                    }else{
                                            console.log(result);
                                            res.render('user/add');
                                    } 
                            }); 
    });

    app.get('/user/cache/read', function(req, res){ 
                            Cache.get('3333333333',function(err,result){ 
                                    if(err || result==undefined){
                                            res.render('404', { error: err});
                                    }else{
                                            console.log(result);
                                            res.render('user/add');
                                    } 
                            }); 
    });

    app.get('/user/cache/update', function(req, res){ 
                            Cache.update('3333333333',"bbbbbbbbbbb",3600,function(err,result){
                                    if(err || result==undefined){
                                            res.render('404', { error: err});
                                    }else{
                                            console.log(result);
                                            res.render('user/add');
                                    } 
                            }); 
    });
    app.get('/user/cache/del', function(req, res){ 
                            Cache.del('3333333333',function(err,result){ 
                                    if(err || result==undefined){
                                            res.render('404', { error: err});
                                    }else{
                                            console.log(result);
                                            res.render('user/add');
                                    } 
                            }); 
    });

}
