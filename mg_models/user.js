/*
    http://mongoosejs.com/docs/queries.html
*/

var User={
     'db':null,

     'model':null,

     'table':'users',

     'field':{
    	    username : {type : String},
    	    email  : {type : String},
    	    avatar  : {type : String},
    	    created  : {type : String}
      },

     'init':function(){
                var mongo=require('./mongo');
                var mg = mongo.init(this.table,this.field);
                this.db=mg.db
                this.model=mg.model;
      },
      
      'create':function(data,callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;
                 var user=new model(data);
            	 user.save(function(err){
                        db.close();
                        var last_id=user._id;
                        callback(err,last_id);
            	 });
      },
      'update':function(conditions, update, options,callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;
            	 model.update(conditions, update, options, function(err){
                        db.close();
                        callback(err);
            	 }); 
       },
      'remove':function(conditions,callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;
            	 model.remove(conditions ,function(err){
                        db.close();
                        callback(err);
            	 });
       },
      'findOne':function(conditions,fields,callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;
            	 model.findOne(conditions, fields, function (err, detail) {
                        db.close();
                        callback(err, detail);
            	 });
       },


       'userList':function(callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;
            	 model.find({ avatar: /default/ })
                .where('username').equals('duyongguang')
				//.where('age').gt(17).lt(66)
				.where('email').in(['duyongguang@126.com', 'duyongguang@sina.cn'])
				.limit(10)
				.sort('-_id')
				.select('username email avatar created')
				.exec( function(err, result){
					    db.close();
                        callback(err, result);
				 }); 
       },

     'getUserById':function(uid,callback){
                 this.init();
                 var db=this.db;
                 var model=this.model;

                 var criteria = {_id : uid};
            	 var fields   = { username : 1,avatar : 1,created : 1};
            	 var options  = {};
            	 model.find(criteria, fields, options, function(err, detail){
                        db.close();
                        //callback(err,detail[0]);
                        callback(null,{username:'杜永光',avatar:'/img/medium_default.png',created:'2012-12-12 12:12:12'});
            	 });
      },
}

module.exports=User;