var mongo = {
 	 'init':function(table,field){
                var Config = require('../config');
                var mongoose = require('mongoose');
                var db= mongoose.createConnection(Config.mongo.host+'/'+Config.mongo.database);
                db.on('error', function(error){
                       console.log(error);
                });
            	var schema = new mongoose.Schema(field);
            	var model = db.model(table, schema);
                return {db:db,model:model};
 	  }
}
module.exports = mongo;