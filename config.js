var config= {

  'port': '3000',
  
  'env': 'development', //['development','production']
  
  'domain': '.express.com',

  'secret': "dyg123456", 

  'cache': 'file', // ['memcache','file']

  'database': {
      'host': '127.0.0.1',
      'user': 'root',
      'password': '123456',
      'database': 'test',
  },

  'mongo':{
        'host':'mongodb://localhost:27017',
        'username':'username',
        'password':'password',
        'database':'test'
   },

  'memcache': {
      'host': '127.0.0.1',
      'port': '11211', 
  },

  'api': {
      'client_host': 'http://127.0.0.1:3000',
      'client_id': '1', 
      'client_secret': 'd3s567jfsd93242315673dw33', 
  },


}

module.exports = config;