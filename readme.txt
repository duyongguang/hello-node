(0)参考代码:
    1.some global things IN app.js
        user auth
        xss
        csrf

    2.cookie 
    http://www.express.com/cookie/add
    http://www.express.com/cookie/get
    http://www.express.com/cookie/delete

    3.session
    http://www.express.com/session/add
    http://www.express.com/session/get
    http://www.express.com/session/delete

    4.cache
    http://www.express.com/user/cache/write
    http://www.express.com/user/cache/read
    http://www.express.com/user/cache/update
    http://www.express.com/user/cache/del

    5.mysql 
    http://www.express.com/user/add
    http://www.express.com/user/edit/2
    http://www.express.com/user/del/2
    http://www.express.com/user/list/1
    http://www.express.com/user/all
    http://www.express.com/login

    6.mongo 
    http://www.express.com/mongo/create
    http://www.express.com/mongo/update
    http://www.express.com/mongo/remove
    http://www.express.com/mongo/findone
    http://www.express.com/mongo/query

    7.restful API & API requests
    http://www.express.com/api/get
    http://www.express.com/api/post
    http://www.express.com/api/put
    http://www.express.com/api/delete

    8.async control flowing example
    http://www.express.com/article/series
    http://www.express.com/article/parallel
    http://www.express.com/article/waterfall
    http://www.express.com/article/map
    http://www.express.com/article/foreach

    9.upload images,cut images
    http://www.express.com/upload

    10.kindeditor
    http://www.express.com/editor


(1)结构树:

--app.js
--config.js.bak   新建config.js本地配置 ,内容参照 config.js.bak 
--package.json
--controllers
--------index.js  
--------user.js
--models
--------mysql.js  	//MYSQL对象
--------file.js   	//物理缓存对象
--------memecache.js	//内存缓存对象
--------func.js 	//常用函数
--------user.js		//对应users表
--------comment.js	//对应comments表
--views
--------layout.ejs
--------user
--------------add.ejs
--------------edit.ejs
--------------view.ejs
--------------list.ejs
--------index
--------------index.ejs
--------------login.ejs
--static
--------images
--------javascripts
--------stylesheets
--tmp // storage cache which file type 
-------------------

(2) config.js 配置:
{
    domain //保存session cookie
    cache //缓存方式，file 物理缓存，memcache内存缓存
    secret //保存session cookie
    database //数据库连接
    mongo //数据库连接
    memcache //memcached 连接
    api //接口地址
}
    

(3) 域名解析 ,当前默认的测试域名 www.express.com
        hosts文件：
        127.0.0.1   www.express.com 

        nginx.conf 文件:
        server
        {
            listen       80;
            server_name www.express.com;

            location / {
                    proxy_pass          http://127.0.0.1:3000;
                    proxy_set_header    X-Real-IP $remote_addr;
                    proxy_read_timeout  700;
                    proxy_set_header    Host $http_host;
                    proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
            }
            location ~ .*\.(gif|jpg|jpeg|png|bmp|swf|js|css|txt|ico)$ {
                    proxy_pass          http://127.0.0.1:3000;
                    proxy_set_header    X-Real-IP $remote_addr;
                    proxy_read_timeout  700;
                    proxy_set_header    Host $http_host;
                    proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;

                    access_log   off; 
                    log_not_found  off;
                    expires      300d;
            }

        }
 



(4)额外的module介绍
      "dependencies": {
        "express": "3.5.0",
        "ejs": "*",
        "express-partials":"*",
        "mysql": "*", 	// https://github.com/felixge/node-mysql
        "async":"*",  	// https://github.com/caolan/async
        "memcached":"*",	// https://github.com/3rd-Eden/node-memcached
        "request":"*",	// https://github.com/mikeal/request
        "connect-multiparty":"*"  //https://github.com/andrewrk/connect-multiparty
        "lodash":"*",
        "node-uuid":"*",
        "gm":"*",
        "mongoose":"3.8.*",
        "xss":"*"
      }

        express:框架
        ejs:模板 
        express-partials:页面layout布局
        mysql:数据库引擎 
        async:异步IO，同步流程
        memecached 内存缓存
        request HTTP请求API
        connect-multiparty  上传图片
        lodash 数组，对象 函数库
        gm 切割图片的包，服务器需要安装 ImageMagick （win|linux）
        mongoose mongodb数据库
        xss 防止CSS攻击